package Response

import (
	"net/http"
)

func ResOKWithData(data interface{}) (int, interface{}) {
	return http.StatusOK, ResWithMC{"0000", "success", data}
}

func ResWithError(code string, message string, data interface{}) (int, interface{}) {
	return http.StatusOK, ResWithMC{code, message, data}
}

func ResWithManual(ResMc interface{}) (int, interface{}) {
	return http.StatusOK, ResMc
}
