package Book

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

func GetBooksByURL(url string) ([]Book, error) {
	//url := "https://scb-test-book-publisher.herokuapp.com/books"
	spaceClient := http.Client{
		Timeout: time.Second * 2, // Maximum of 2 secs
	}

	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		log.Fatal(err)
		return []Book{}, err
	}

	res, getErr := spaceClient.Do(req)
	if getErr != nil {
		log.Fatal(getErr)
		return []Book{}, getErr
	}

	body, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		log.Fatal(readErr)
		return []Book{}, readErr
	}

	var book []Book
	jsonErr := json.Unmarshal(body, &book)
	if jsonErr != nil {
		log.Fatal(jsonErr)
		return []Book{}, jsonErr

	}
	return book, nil
}

func GetBookSAll() ([]Book, error) {
	var Books []Book
	if BookRecom, err := GetBooksByURL("https://scb-test-book-publisher.herokuapp.com/books/recommendation"); err != nil {
		return []Book{}, err
	} else {
		Books = BookRecom
	}

	if Bookdata, err := GetBooksByURL("https://scb-test-book-publisher.herokuapp.com/books"); err != nil {
		return []Book{}, err
	} else {
		for _, api := range Bookdata {
			var checkdup bool = false
			for _, book := range Books {
				if api.Id == book.Id {
					checkdup = true
				}
			}
			if !checkdup {
				Books = append(Books, api)
			}
		}
	}
	return Books, nil
}
