package Book

type Book struct {
	Id         int     `json:"id"`
	Price      float32 `json:"price"`
	AuthorName string  `json:"author_name"`
	BookName   string  `json:"book_name"`
}
