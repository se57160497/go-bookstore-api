package Book

import (
	"github.com/gin-gonic/gin"
	"go-bookstore-api/Response"
)

func GetAllBook(c *gin.Context) {

	if Book, err := GetBookSAll(); err != nil {
		c.JSON(Response.ResWithError("book01", "Cannot list books data, Please try again.", err.Error()))
		return
	} else {
		c.JSON(Response.ResOKWithData(Book))
		return
	}
	return
}
