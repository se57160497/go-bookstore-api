package Auth

import (
	"encoding/gob"
	"encoding/json"
	"fmt"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"github.com/thoas/go-funk"
	"go-bookstore-api/User"
	"net/http"
)

func AuthenticationRequired(auths ...string) gin.HandlerFunc {
	return func(c *gin.Context) {
		fmt.Println(c)
		session := sessions.Default(c)
		user := session.Get("user")
		fmt.Println("USER : ", session)
		if user == nil {
			c.JSON(http.StatusUnauthorized, gin.H{"error": "user needs to be signed in to access this service"})
			c.Abort()
			return
		} else {
			var ses User.UserWithoutPass
			byteData, _ := json.Marshal(user)
			json.Unmarshal(byteData, &ses)
			if err := ses.GetUserByUsername(); err != nil {
				session.Delete("user")
				if err := session.Save(); err != nil {
					fmt.Println("ERROR SAVE SESSION : ", err)
				}
				c.JSON(http.StatusUnauthorized, gin.H{"error": "User not found, Please login and try again."})
				c.Abort()
				return
			} else {
				gob.Register(&User.UserWithoutPass{})
				session.Set("user", ses)
				if err := session.Save(); err != nil {
					fmt.Println("ERROR SAVE SESSION : ", err)
				}
			}
		}
		if len(auths) != 0 {
			authType := session.Get("authType")
			if authType == nil || !funk.ContainsString(auths, authType.(string)) {
				c.JSON(http.StatusForbidden, gin.H{"error": "invalid request, restricted endpoint"})
				c.Abort()
				return
			}
		}
		// add session verification here, like checking if the user and authType
		// combination actually exists if necessary. Try adding caching this (redis)
		// since this middleware might be called a lot
		c.Next()
	}
}
