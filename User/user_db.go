package User

import (
	"errors"
	"fmt"
	"go-bookstore-api/Database"
	"go.mongodb.org/mongo-driver/bson"
	"log"
)

func GetAllUserDB() ([]User, error) {
	collection, ctx := Database.ConnectDB("go-bookstore", "users")
	filter := bson.M{}
	cur, err := collection.Find(ctx, filter)
	if err != nil {
		log.Fatal(err)
	}
	defer cur.Close(ctx)
	var data []User

	for cur.Next(ctx) {
		var subData User
		err := cur.Decode(&subData)
		if err != nil {
			log.Fatal(err)
		}
		data = append(data, subData)
	}
	if err := cur.Err(); err != nil {
		log.Fatal(err)
	}
	fmt.Println(data)
	return data, nil
}

func CreateUserDB(req User) error {
	collection, ctx := Database.ConnectDB("go-bookstore", "users")
	req.Password, _ = HashPassword(req.Password)
	checkdup := bson.M{"username": req.Username}
	if count, _ := collection.CountDocuments(ctx, checkdup); count > 0 {
		return errors.New("User Duplicated, Please Try again.")
	}
	if rtn, err := collection.InsertOne(ctx, req); err != nil {
		fmt.Println(err)
		fmt.Println(rtn)
		return err
	} else {
		fmt.Println(rtn)
	}
	return nil
}

func UpdateUserDB(req UserWithoutPass) error {
	collection, ctx := Database.ConnectDB("go-bookstore", "users")
	checkdup := bson.M{"username": req.Username}
	set := bson.M{"$set": bson.M{"books": req.Books}}
	if count, _ := collection.CountDocuments(ctx, checkdup); count <= 0 {
		return errors.New("Not found User, Please Try again.")
	}
	if rtn, err := collection.UpdateOne(ctx, checkdup, set); err != nil {
		fmt.Println(err)
		fmt.Println(rtn)
		return err
	} else {
		fmt.Println(rtn)
	}
	return nil
}

func (user *UserWithoutPass) GetUserByUsername() error {
	collection, ctx := Database.ConnectDB("go-bookstore", "users")
	checkdup := bson.M{"username": user.Username}
	if count, err := collection.CountDocuments(ctx, checkdup); count <= 0 {
		return errors.New("Wrong Username or Password, Please try again.")
	} else if err != nil {
		return err
	} else {
		if err := collection.FindOne(ctx, checkdup).Decode(&user); err != nil {
			return err
		}
	}
	return nil
}

func (user *User) LoginUserDB() error {
	collection, ctx := Database.ConnectDB("go-bookstore", "users")
	var RawPassword = user.Password
	checkdup := bson.M{"username": user.Username}
	if count, err := collection.CountDocuments(ctx, checkdup); count <= 0 {
		return errors.New("Wrong Username or Password, Please try again.")
	} else if err != nil {
		return err
	} else {
		if err := collection.FindOne(ctx, checkdup).Decode(&user); err != nil {
			return err
		} else {
			if !CheckPasswordHash(RawPassword, user.Password) {
				return errors.New("Wrong Username or Password, Please try again.")
			}
		}
	}
	return nil
}

func (user *UserWithoutPass) DeleteUserDB() error {
	collection, ctx := Database.ConnectDB("go-bookstore", "users")
	checkdup := bson.M{"username": user.Username}
	if rtn, err := collection.DeleteOne(ctx, checkdup); err != nil {
		fmt.Println(err)
		fmt.Println(rtn)
		return err
	}
	return nil
}
