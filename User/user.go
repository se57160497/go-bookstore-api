package User

import (
	"encoding/gob"
	"encoding/json"
	"fmt"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"go-bookstore-api/Book"
	"go-bookstore-api/Response"
	"net/http"
)

func CreateUser(c *gin.Context) {
	var req User
	if err := c.ShouldBindJSON(&req); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	if err := CreateUserDB(req); err != nil {
		c.JSON(Response.ResWithError("user01", "Cannot create user ,Please try again.", err.Error()))
		return
	}
	c.JSON(Response.ResOKWithData(nil))
	return
}

func LoginUser(c *gin.Context) {
	var req User
	if err := c.ShouldBindJSON(&req); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	if err := req.LoginUserDB(); err != nil {
		c.JSON(Response.ResWithError("user02", "Cannot login this user ,Please try again.", err.Error()))
		return
	} else {
		session := sessions.Default(c)
		ses := UserWithoutPass{req.Username, req.FirstName, req.SurName, req.DOB, req.Books}
		gob.Register(&UserWithoutPass{})
		session.Set("user", ses)
		if err := session.Save(); err != nil {
			fmt.Println("ERROR SAVE SESSION : ", err)
		}
		c.JSON(Response.ResOKWithData(ses))
		return
	}
}

func GetDetailUser(c *gin.Context) {
	session := sessions.Default(c)
	user := session.Get("user")
	c.JSON(Response.ResOKWithData(user))
	return
}

func DeleteUser(c *gin.Context) {
	session := sessions.Default(c)
	user := session.Get("user")
	var req UserWithoutPass
	byteData, _ := json.Marshal(user)
	json.Unmarshal(byteData, &req)
	if err := req.DeleteUserDB(); err != nil {
		c.JSON(Response.ResWithError("user03", "Cannot delete this user ,Please try again.", err.Error()))
		return
	}
	session.Delete("user")
	if err := session.Save(); err != nil {
		fmt.Println("ERROR SAVE SESSION : ", err)
	}
	c.JSON(Response.ResOKWithData(req))
	return
}

func LogoutUser(c *gin.Context) {
	session := sessions.Default(c)
	session.Delete("user")
	if err := session.Save(); err != nil {
		fmt.Println("ERROR SAVE SESSION : ", err)
	}
	c.JSON(Response.ResOKWithData(""))
	return
}

func OrderBookUser(c *gin.Context) {
	var req struct {
		Orders []int `json:"orders"`
	}
	if err := c.ShouldBindJSON(&req); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	session := sessions.Default(c)
	user := session.Get("user")
	var ses UserWithoutPass
	byteData, _ := json.Marshal(user)
	json.Unmarshal(byteData, &ses)

	var Books []Book.Book
	if Book, err := Book.GetBookSAll(); err != nil {
		c.JSON(Response.ResWithError("OrderBook01", "Cannot list books data, Please try again.", err.Error()))
		return
	} else {
		Books = Book
	}

	var price float32 = 0
	for _, ListOrder := range req.Orders {
		var CheckDup bool = false
		for _, api := range Books {
			if ListOrder == api.Id {
				price += api.Price
				CheckDup = true
			}
		}
		if !CheckDup {
			c.JSON(Response.ResWithError("OrderBook02", "Not found about books in list order, Please try again.", ""))
			return
		}
	}
	ses.Books = append(ses.Books, req.Orders...)

	if err := UpdateUserDB(ses); err != nil {
		c.JSON(Response.ResWithError("OrderBook03", "Cannot Update Books in User, Please try again.", err.Error()))
		return
	}
	res := struct {
		Orders []int   `json:"orders"`
		Price  float32 `json:"price"`
	}{ses.Books, price}
	c.JSON(Response.ResOKWithData(res))
	return
}
