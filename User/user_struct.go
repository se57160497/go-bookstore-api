package User

import "golang.org/x/crypto/bcrypt"

type User struct {
	Username  string `json:"username" bson:"username" binding:"required"`
	Password  string `json:"password" bson:"password" binding:"required"`
	FirstName string `json:"name" bson:"name"`
	SurName   string `json:"surname" bson:"surname"`
	DOB       string `json:"date_of_birth" bson:"date_of_birth"`
	Books     []int  `json:"books" bson:"books"`
}

type UserWithoutPass struct {
	Username  string `json:"username" bson:"username" binding:"required"`
	FirstName string `json:"name" bson:"name"`
	SurName   string `json:"surname" bson:"surname"`
	DOB       string `json:"date_of_birth" bson:"date_of_birth"`
	Books     []int  `json:"books" bson:"books"`
}

func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}
